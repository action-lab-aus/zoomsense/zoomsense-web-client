import Vue from "vue";
import App from "./App.vue";
import { rtdbPlugin } from "vuefire";
import "./scripts/gapi.js";
import VueCookies from "vue-cookies";
import axios from "axios";
import VueAxios from "vue-axios";
import { Timeline, Network } from "vue-visjs";
import AsyncComputed from "vue-async-computed";
import VueClipboard from "vue-clipboard2";

import router from "./router";
import "./quasar";

Vue.config.productionTip = false;

Vue.component("timeline", Timeline);
Vue.component("network", Network);

Vue.use(rtdbPlugin);
Vue.use(VueCookies);
Vue.use(VueAxios, axios);
Vue.use(AsyncComputed);
Vue.use(VueClipboard);
Vue.use(router);

new Vue({
  router,
  render: (h) => h(App),
}).$mount("#app");
