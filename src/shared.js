import { auth } from "./db";
import stringHash from "string-hash";

export const getSessionToken = async () => {
  return auth.currentUser.getIdToken();
};

const ONE_MINUTE = 1000 * 60;

/**
 * Given a history string, we round it to within a minute.
 *
 * This allows us to consider two breakout rooms that ended within
 * a minute of each other to be part of the same 'timeline' item
 */
export const roundHistory = historyString =>
  Math.floor(parseInt(historyString) / ONE_MINUTE) * ONE_MINUTE;

/**
 * Given a rounded history string and history object, who's keys
 * are actual history values, get the real history value that
 * corresponds to the rounded history string
 */
export const lookupRealHistory = (roundedHistoryString, history) => {
  // loop through and find the key that matches:
  if (roundedHistoryString && history) {
    for (let histroryKey in history) {
      if (roundHistory(histroryKey) == parseInt(roundedHistoryString)) {
        return histroryKey;
      }
    }
    return -1;
  } else {
    return 0;
  }
};

/**
 * Given a `filename` and some `text`, download the `text` to
 * the users filesystem as a file named `filename`
 */
export const download = (filename, text) => {
  var element = document.createElement("a");
  element.setAttribute(
    "href",
    "data:text/plain;charset=utf-8," + encodeURIComponent(text),
  );
  element.setAttribute("download", filename);

  element.style.display = "none";
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
};

/**
 * This is a list of quasar colors that are appropriate
 * to have black text rendered over them.
 *
 * As defined by https://quasar.dev/style/color-palette#color-list
 */
export const COLOUR_ARRAY = [
  "red-13",
  "orange-13",
  "grey-11",
  "deep-purple-3",
  "pink-11",
  "purple-11",
  "cyan-11",
  "brown-11",
  "deep-purple-1",
  "deep-orange-2",
  "teal-3",
  "cyan-3",
  "teal-12",
  "cyan-14",
  "teal-2",
  "blue-5",
  "blue-grey-4",
  "yellow-2",
  "green-13",
  "light-green-5",
  "deep-purple-12",
  "red-12",
  "teal-4",
  "indigo-13",
  "teal-11",
  "deep-orange-11",
  "deep-purple-14",
  "lime-12",
  "light-green-4",
  "pink-4",
  "blue-4",
  "purple-1",
  "brown-3",
  "green-5",
  "indigo-12",
  "deep-orange-14",
  "light-blue-5",
  "purple-5",
  "light-blue-1",
  "light-green-12",
  "amber-13",
  "light-green-3",
  "pink-2",
  "light-blue-4",
  "light-green-2",
  "light-blue-2",
  "blue-grey-14",
  "light-green-13",
  "amber-11",
  "light-blue-3",
  "blue-grey-5",
  "deep-orange-13",
  "green-14",
  "green-11",
  "blue-grey-2",
  "yellow-14",
  "yellow-1",
  "blue-2",
  "amber-4",
  "light-green-14",
  "orange-12",
  "purple-4",
  "red-5",
  "green-4",
  "lime-2",
  "indigo-2",
  "grey-14",
  "orange-2",
  "teal-5",
  "blue-1",
  "amber-5",
  "lime-1",
  "brown-14",
  "red-3",
  "deep-orange-1",
  "pink-14",
  "deep-orange-4",
  "yellow-13",
  "green-1",
  "light-green-11",
  "red-4",
  "amber-1",
  "teal-14",
  "light-green-1",
  "brown-13",
  "brown-5",
  "pink-5",
  "yellow-4",
  "orange-5",
  "green-12",
  "lime-5",
  "grey-13",
  "teal-1",
  "amber-2",
  "deep-orange-3",
  "cyan-13",
  "purple-12",
  "purple-14",
  "teal-13",
  "pink-1",
  "yellow-12",
  "red-14",
  "light-blue-14",
  "deep-orange-5",
  "blue-12",
  "orange-3",
  "orange-11",
  "purple-13",
  "indigo-4",
  "light-blue-12",
  "lime-14",
  "blue-13",
  "deep-orange-12",
  "blue-14",
  "cyan-12",
  "grey-3",
  "blue-3",
  "cyan-4",
  "yellow-11",
  "grey-5",
  "grey-4",
  "lime-13",
  "grey-12",
  "light-blue",
  "red-2",
  "cyan-1",
  "deep-purple-13",
  "brown-12",
  "blue-grey-1",
  "indigo-3",
  "yellow-3",
  "yellow-5",
  "orange-4",
  "orange-14",
  "lime-11",
  "pink-3",
  "red-11",
  "lime-3",
  "brown-1",
  "deep-purple-2",
  "pink-12",
  "deep-purple-5",
  "lime-4",
  "indigo-5",
  "indigo-14",
  "brown-4",
  "blue-grey-3",
  "grey-2",
  "indigo-11",
  "blue-grey-12",
  "amber-12",
  "purple-3",
  "deep-purple-11",
  "green-3",
  "green-2",
  "blue-11",
  "orange-1",
  "amber-3",
  "red-1",
  "purple-2",
  "pink-13",
  "light-blue-13",
  "blue-grey-11",
  "light-blue-11",
  "indigo-1",
  "grey-1",
  "deep-purple-4",
  "blue-grey-13",
  "cyan-2",
  "amber-14",
  "brown-2",
];

/**
 * Given a string, return a color from the COLOUR_ARRAY
 *
 * We use the string-hash library to hash the string and
 * select a colour
 */
export const stringToColor = string =>
  COLOUR_ARRAY[stringHash(string) % COLOUR_ARRAY.length];

/**
 * Given a string and a set of taken colors, return
 * a color that isn't taken. If all colors are in
 * the set, return the first color that corresponds to the
 * string
 */
export const getUniqueColorForString = (string, colourSet) => {
  const stringHashValue = stringHash(string);
  let hashOffset = 0;
  let newColor = "";
  do {
    newColor =
      COLOUR_ARRAY[(stringHashValue + hashOffset) % COLOUR_ARRAY.length];
    hashOffset++;
  } while (colourSet.has(newColor) && colourSet.size < COLOUR_ARRAY.length);
  return newColor;
};
