// import Vue from "vue";
import moment from "moment";
import { db, config, functions } from "../db";
const generateLink = functions.httpsCallable("generateLink");

export const setMeetingScheduleManual = async function(
  VueInstance,
  uid,
  meetingIn,
) {
  try {
    let resp = await VueInstance.$http.post(
      `${config.functions_url}/getMeetingCapacityManual`,
      {
        duration:
          parseInt(meetingIn.meetingLength.split(":")[0]) +
          meetingIn.meetingLength.split(":")[1] / 60.0,
        start_time: meetingIn.startTime,
      },
    );

    if (resp.status != 200) {
      throw new Error(resp.statusText);
    }

    let meeting = { ...meetingIn };
    delete meeting.startTimeT;
    delete meeting.meetingLength;
    if (meeting.password == "") delete meeting.password;

    let capacity = resp.data.capacity;
    // capacity = 0;

    // If no capacity, display the warning dialog
    if (capacity <= 0) {
      await new Promise(resolve =>
        VueInstance.$q
          .dialog({
            title: "No Capacity",
            persistent: true,
            message: `We don't have any ZoomSensors available for ${
              meeting.topic
            } at ${moment(meeting.startTime).format("hh:mm")} on ${moment(
              meeting.startTime,
            ).format(
              "DD/MM/YY",
            )}, please contact tom.bartindale@monash.edu for any further assistance.`,
            // cancel: true,
          })
          .onOk(() => resolve(true))
          .onCancel(() => resolve(false)),
      );

      throw new Error("No capacity");
    }

    // If has capacity but less than the requested no. of breakout rooms, check with
    // the host whether he/she wants to continue the available no. of sensors
    else if (capacity > 0 && capacity < meeting.noOfBreakoutRooms) {
      let cont = await new Promise(resolve =>
        VueInstance.$q
          .dialog({
            title: "Limited Capacity",
            cancel: {
              label: "Don't Schedule",
              flat: true,
            },
            ok: {
              label: "Proceed",
            },
            persistent: true,
            message: `We only have ${capacity} ZoomSensor${
              capacity > 1 ? "s" : ""
            } available for ${meeting.topic} at ${moment(
              meeting.startTime,
            ).format("hh:mm")} on ${moment(meeting.startTime).format(
              "DD/MM/YY",
            )}. Please contact tom.bartindale@monash.edu for any further assistance. Proceed anyway?`,
            // cancel: true,
          })
          .onOk(() => resolve(true))
          .onCancel(() => resolve(false)),
      );

      if (!cont) throw new Error("Limited capacity, don't proceed");
      //set capacity to be what is available:
      meeting.noOfBreakoutRooms = capacity;
    }
    // Otherwise, successfully schedule the sensors
    const startTime = meeting.startTime
      .replaceAll("-", "")
      .replaceAll(":", "")
      .split(".")[0];
    const meetingKey = `${meeting.id}_${startTime}`;
    meeting.scheduledManually = true;
    const result = await generateLink({
      meetingid: meetingKey,
      hostuid: uid,
    });
    meeting.shareLink = `${process.env.VUE_APP_REDIRECT}/#/anonymous?token=${result.data}`;
    await db.ref(`/meetings/${uid}/${meetingKey}`).set(meeting);

    // If webinar, check whether the token has been retrieved
    if (meeting.type === "webinar") {
      const webinarToken = await db
        .ref(`/webinarToken/${meeting.id}`)
        .once("value");

      // If token exists, set the token to the meeting object
      if (webinarToken.val())
        await db
          .ref(`/meetings/${uid}/${meetingKey}/webinarToken`)
          .set(webinarToken.val().token);
      // Otherwise, set the scheduled flag temporarily to true to avoid
      // sensors being scheduled until the panelist invitation email is
      // received and parsed
      else await db.ref(`/meetings/${uid}/${meetingKey}/scheduled`).set(true);
    }
  } catch (error) {
    //we assume this is an auth error and the whole thing should be canned
    console.log("error: ", error);
    throw error;
  }
};
