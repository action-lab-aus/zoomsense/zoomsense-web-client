import * as firebase from "firebase/app";
import * as firebaseui from "firebaseui";
import "firebase/database";
import "firebase/auth";
import "firebase/storage";
import "firebase/functions";
import "firebaseui";
import "firebaseui/dist/firebaseui.css";

const firebaseConfig = {
  apiKey: `${process.env.VUE_APP_FB_KEY}`,
  authDomain: `${process.env.VUE_APP_FB_AUTH}`,
  databaseURL: `${process.env.VUE_APP_RTDB}`,
  projectId: `${process.env.VUE_APP_FB_ID}`,
  storageBucket: `${process.env.VUE_APP_FB_BUCKET}`,
  functions_url: `${process.env.VUE_APP_FB_FUNCTIONS}`,
  messagingSenderId: `${process.env.VUE_APP_MESSAGING_SENDER_ID}`,
  appId: `${process.env.VUE_APP_APP_ID}`,
};

firebase.initializeApp(firebaseConfig);

if (process.env.VUE_APP_USE_EMULATOR) {
  firebase.functions().useFunctionsEmulator("http://localhost:5001");
  firebase.auth().useEmulator("http://localhost:9099");
}

export const db = firebase.database();
export const auth = firebase.auth();
export const storage = firebase.storage();
export const functions = firebase.functions();
export const config = firebaseConfig;
export const fbUi = new firebaseui.auth.AuthUI(auth);
export const googleProvider = firebase.auth.GoogleAuthProvider;
