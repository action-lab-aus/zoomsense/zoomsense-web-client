export default {
  methods: {
    showError(error) {
      this.$q.notify({
        type: "negative",
        message: error,
        position: "top",
      });
    },
  },
};
