# ZoomSense Web Client

The ZoomSense management web client is implemented as a static VueJS browser application to display data collected by ZoomSensors operating inside specified Zoom meetings, allowing for greater insight into what's happening in your Zoom meetings and their breakout rooms.

# Getting Started

## Prerequisites

- [Firebase Project](https://gitlab.com/action-lab-aus/zoomsense/zoomsense/-/blob/master/docs/2_QuickStart_Firebase.md) with **Blaze Plan (Pay as you go)**
- [Zoom OAuth App](https://gitlab.com/action-lab-aus/zoomsense/zoomsense/-/blob/master/docs/3_QuickStart_Zoom_OAuth.md)

## 1. Create a Firebase Project

A quick overview of how to create a Firebase Project for ZoomSense with a Realtime Database, Storage, Authentication can be found under the [ZoomSense QuickStart Guide](https://gitlab.com/action-lab-aus/zoomsense/zoomsense/-/blob/master/docs/2_QuickStart_Firebase.md).

## 2. Generate **Zoom OAuth** Key and Secret

Go to [Zoom MarketPlace](https://marketplace.zoom.us/) and create a Zoom account.

Under the **Develop** tab, select **Build App**. Choose Oauth to start creating an OAuth App on Zoom:

- Provide the OAuth App Name
- Choose the app type to be **User-managed app**
- Deselect publishing to Zoom Marketplace
- Click **Create** to create the OAuth App

### 2.1 Configure Redirect and Whitelist URL for OAuth

You will be provided with Zoom OAuth Credentials for accessing Zoom APIs. Under the **App Credentials** section, fill in the **Redirect URL for OAuth** & **Add Whitelist URLs** with:

```
https://us-central1-FIREBASE_PROJECT_ID.cloudfunctions.net/authtokenredirect
```

The FIREBASE_PROJECT_ID can be found in the Firebase console, under **Project settings**'s **General** section.

### 2.2 Configure Zoom OAuth App Scopes

Leave the default details under the **Information** and **Feature** section, and go to the **Scopes** section. Add the following scopes via the console so that we will be able to retrieve the user and meeting details on behalf of the user:

- `meeting:read`
- `meeting:write`
- `user:read`
- `user_profile`

## 3. Set up Firebase Function Configs for Zoom OAuth Credentials

Run `firebase use --add` to define the project alias to be used for the Web Client (set the alias to be **default**).

Execute the following command to set up Firebase function configs with **Zoom OAuth Credentials** you obtained.

```
firebase functions:config:set FIREBASE_PROJECT_ID.zoom.client_id="Zoom OAuth ID" FIREBASE_PROJECT_ID.zoom.client_secret="Zoom OAuth Secret"
```

## 4. Create a Firebase Web App

Before deploying the ZoomSense Web Client to Firebase, we need to create a Firebase Web App to get started. Under the Firebase console in the settings tab, register a web app by providing the App nickname. Choose **Also set up Firebase Hosting for this app** to get Firebase Hosting setup for you.

Choose **Continue** for all the remaining sections, and you will be directed back to the **Project settings** tab. Under **Your app**, you will see a Web App created successfully. Choose **Config** under the **SDK setup and configuration** section to get the Firebase configuration object containing keys and identifiers for your app.

## 5. Set up the Firebase Configurations

Update the `.env.production` & `.env.development` files with the configuration details received in the previous step as:

```
VUE_APP_REDIRECT=https://FIREBASE_PROJECT_ID.web.app
API_KEY=firebaseConfig.apiKey
AUTH_DOMAIN=firebaseConfig.authDomain
DATABASE_URL=firebaseConfig.databaseURL
PROJECT_ID=firebaseConfig.projectId
STORAGE_BUCKET=firebaseConfig.storageBucket
FUNCTIONS_URL=https://us-central1-FIREBASE_PROJECT_ID.cloudfunctions.net
MESSAGING_SENDER_ID=firebaseConfig.messagingSenderId
APP_ID=firebaseConfig.appId
```

To run the Firebase project using local emulators, please make sure the following three environment variables are updated accordingly:

```
VUE_APP_RTDB=http://localhost:9000/?ns=FIREBASE_PROJECT-default-rtdb
VUE_APP_FB_FUNCTIONS=http://localhost:5001/FIREBASE_PROJECT/us-central1
VUE_APP_USE_EMULATOR=true
```

These environment variables will then be loaded into `src/db.js` for initializing the Firebase App for the Web Client.

## 6. Run the Web Client Locally

Make sure under the `.firebaserc` file, the default project has been set to the Firebase Project you have created as:

```
{
  "projects": {
    "default": "zoomsense-acmmm"
  }
}
```

Install dependencies:

```
npm install
```

Compiles and hot-reloads for development:

```
npm run serve
```

## 7. Build and Deploy the Web Client

Build the Web Client and deploy it to Firebase Hosting:

```
npm run build

firebase deploy
```
